import React, { useEffect, useState } from 'react';


function LocationForm() {
    // const [states, setStates] = useState([]); create a state variable called states and a function called setStates to update the state variable
    const [states, setStates] = useState([]);


    const  fetchData = async () => {
        const url = 'http://localhost:8000/api/states';
        

        // try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            // // Get the select tag element by its id 'state'
            // const selectTag = document.getElementById("state");
            // // For each state in the states property of the data
            // for (let state of data.states) {
            //     // Create an 'option' element
            //     const option = document.createElement("option");
            //     // Set the '.value' property of the option element to the
            //     // state's abbreviation
            //     option.value = state.abbreviation;
            //     // Set the '.innerHTML' property of the option element to
            //     // the state's name
            //     option.innerHTML = state.name;
            //     // Append the option element as a child of the select tag
            //     selectTag.appendChild(option);

        }
       

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (

        // return method
        <>
            
            <p>Location Form</p><select required name="state" id="state" className="form-select">
                <option value="">Choose a state</option>
                {states.map(state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                    );
                })}
            </select>
        </>
    )
}

export default LocationForm;