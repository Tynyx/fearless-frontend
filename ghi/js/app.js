function createCard(name, description, pictureUrl, date, dateEnd, location) {
    
    return `
        <div class="card" style="
            width: 18rem;
            margin: 1rem;
            display: inline-block;
            background-color: #f8f9fa;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
            border-radius: 10px;
        
        
        
        
        ">
            <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-subtitle mb-2 text-muted">${location}</p>
                    <p class="card-text">${description}</p>
                    <p class="card-footer">${date}-${dateEnd}</p>
                </div>
        </div>

    `;
    
        
}






window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';



    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
            throw new Error(`HTTP error! status: ${response.status}`);
        } else {
            const data = await response.json();
            

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const date = details.conference.starts;
                    const dateEnd = details.conference.ends;
                    // Assuming date and dateEnd are in ISO format (yyyy-mm-dd)
                    const dateObject = new Date(date);
                    const dateEndObject = new Date(dateEnd);

                    // Format the date in mm/dd/yyyy format
                    const formattedDate = `${dateObject.getMonth() + 1}/${dateObject.getDate()}/${dateObject.getFullYear()}`;
                    const formattedDateEnd = `${dateEndObject.getMonth() + 1}/${dateEndObject.getDate()}/${dateEndObject.getFullYear()}`;

                    // Now you can use formattedDate and formattedDateEnd
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, formattedDate, formattedDateEnd, location);
                    const column = document.querySelector('.col');
                    column.innerHTML += html;
                    console.log(html);
                    const row = document.querySelector('.row');
                    row.appendChild(column);
                }
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
        console.error(e);
        const alert = document.createElement('div');
        alert.className = 'alert alert-danger';
        alert.role = 'alert';
        alert.innerText = 'There was an error: ' + e.message;

        document.body.appendChild(alert);

        setTimeout(() => {
            document.body.removeChild(alert);
        }, 5000);  // Remove the alert after 5 seconds
    }

});

